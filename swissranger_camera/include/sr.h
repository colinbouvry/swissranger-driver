
#ifndef SR_HH
#define SR_HH

#include <libMesaSR.h>

// ROS include
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <swissranger_camera/ExposureTime.h>

#define USE_SR4K 1 // Comment this out for SR3K
#define USE_FILTER 1

#define SR_IMG_DISTANCE 0
#define SR_IMG_AMPLITUDE 1
#define SR_IMG_CONFIDENCE 2

// This is setting all of the possible modes
// #define SR4K_MODE (AM_CONF_MAP | AM_CONV_GRAY | AM_COR_FIX_PTRN | AM_DENOISE_ANF)
// Here are some of the most important enumerations for the SR_SetMode function:
// AM_Denoise_ANF :turns on a hardware-implemented noise filter.
// This filter combines amplitudeand distance information in a 5 by 5neighbourhood.  
// It reduces noise while preserving detail such as edges or small structures, with no computational cost to the host CPU.
// This filter is on by default
// AM_SW_ANF : turns on a 7x7 software filter on the same principle than the hardware DenoiseANF filter. 
// This filter preserves less detail, and might therefore not be ideal for certain applications.
// AM_Median: turns on a 3x3 median filter that filters extreme values out and that is particularly usefull when measuring smooth ssurfaces with only few small details.
// AM_Confidence Map:turns on the generation a the Confidence Map.  
// If the Confidence Map is not used in an application, one can save considerable host CPU processing resources by switching this option of.
// AM_Short Range: As explained in ‘Focus Adjustment’, in this mode the coordinate transform uses the actual measured distance to determine the focal point for the transformation.
// This assumes that the camera is focused at the measured distance.

//#define SR4K_MODE (AM_CONF_MAP | AM_CONV_GRAY)
#define SR4K_MODE (AM_CONF_MAP | AM_CONV_GRAY | AM_DENOISE_ANF | AM_SW_ANF | AM_MEDIANCROSS)
#define SR3K_MODE (AM_COR_FIX_PTRN | AM_MEDIAN)

#ifdef USE_SR4K
#define MODE SR4K_MODE
#else
#define MODE SR3K_MODE
#endif

namespace sr
{

using namespace std;
//! Macro for defining an exception with a given parent (std::runtime_error should be top parent)
// code borrowed from drivers/laser/hokuyo_driver/hokuyo.h
#define DEF_EXCEPTION(name, parent)        \
  class name : public parent               \
  {                                        \
  public:                                  \
    name(const char *msg) : parent(msg) {} \
  }

//! A standard SR exception
DEF_EXCEPTION(Exception, std::runtime_error);

const int SR_COLS = 176;
const int SR_ROWS = 144;
#ifdef USE_SR4K
const int SR_IMAGES = 3;
#else
const int SR_IMAGES = 2;
#endif

class SR
{
public:
  SR(bool use_filter = USE_FILTER);
  ~SR();

  int open(int auto_exposure, int integration_time,
           int modulation_freq, int amp_threshold, std::string &ether_addr, unsigned int serial_number = 0);
  int close();

  void readData(sensor_msgs::PointCloud &cloud,
                sensor_msgs::PointCloud2 &cloud2,
                sensor_msgs::Image &image_d,
                sensor_msgs::Image &image_i,
                sensor_msgs::Image &image_c,
                sensor_msgs::Image &image_d16,
                sensor_msgs::Image &image_i16,
                swissranger_camera::ExposureTime &exposure_info);

  int setAutoExposure(bool on);
  int setIntegrationTime(int time);
  int getIntegrationTime();
  int setModulationFrequency(int freq);
  int getModulationFrequency();
  int setAmplitudeThreshold(int thresh);
  int getAmplitudeThreshold();

  std::string device_id_;
  std::string lib_version_;

private:
  // device identifier
  CMesaDevice *srCam_;

  ImgEntry *imgEntryArray_;
  float *buffer_, *xp_, *yp_, *zp_;

  int integration_time_, modulation_freq_;

  bool use_filter_;

  std::string getDeviceString();
  std::string getLibraryVersion();

  void SafeCleanup();
};
}; // namespace sr

#endif
